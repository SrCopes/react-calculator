/* eslint-disable no-eval */
import React, { useState } from "react";
import "../App.css";
import CalculateIcon from "@mui/icons-material/Calculate";
import BackspaceIcon from "@mui/icons-material/Backspace";
import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline";
import DoDisturbOnIcon from "@mui/icons-material/DoDisturbOn";
import { FiDivideCircle } from "react-icons/fi";
import { AiOutlineCloseCircle } from "react-icons/ai";

export default function Calculator() {
  const [calculation, setCalculation] = useState("");
  const [result, setResult] = useState("");

  const operator = [".", "+", "-", "/", "*"];

  let updateCalculations = (val) => {
    if (
      (operator.includes(val) && calculation === "") || //if is empty the calc
      (operator.includes(val) && operator.includes(calculation.slice(-1))) //last value of the calc
    ) {
      return;
    }

    setCalculation(calculation + val);

    if (!operator.includes(val)) {
      setResult(eval(calculation + val).toString());
    }
  };

  let updateOnEqualBtt = () => {
    setCalculation(eval(calculation).toString());
  };

  let deleteAnElement = () => {
    if (calculation === "") {
      return;
    }
    const newVal = calculation.slice(0, -1);

    setCalculation(newVal);
  };

  const putDigitsToButtons = () => {
    const digits = [];

    for (let i = 0; i < 10; i++) {
      digits.push(
        <button key={i} onClick={() => updateCalculations(i.toString())}>
          {i}
        </button>
      );
    }
    return digits;
  };

  return (
    <div className="App">
      <div className="calc">
        <div className="res">
          {result ? <span>[{result}]</span> : ""}
          {calculation || "0"}
        </div>
        <div className="operatButts">
          <button onClick={() => updateCalculations("+")}>
            <AddCircleOutlineIcon />
          </button>
          <button onClick={() => updateCalculations("*")}>
            <AiOutlineCloseCircle />
          </button>
          <button onClick={() => updateCalculations("-")}>
            <DoDisturbOnIcon />
          </button>
          <button onClick={() => updateCalculations("/")}>
            <FiDivideCircle />
          </button>
          <button onClick={deleteAnElement}>
            <BackspaceIcon />
          </button>
        </div>
        <div className="digits">
          {putDigitsToButtons()}
          <button onClick={() => updateCalculations("0")}>0</button>
          <button onClick={() => updateCalculations(".")}>.</button>
          <button onClick={updateOnEqualBtt}>
            <CalculateIcon />
          </button>
        </div>
      </div>
    </div>
  );
}
