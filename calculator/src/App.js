import "./App.css";
import Calculator from "./components/Calculator";

export default function App() {
  return (
    <>
      <header>
        <h1>My Calculator</h1>
      </header>
      <div>
        <Calculator />
      </div>
    </>
  );
}
